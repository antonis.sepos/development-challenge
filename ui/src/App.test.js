import { render, waitFor, screen } from '@testing-library/react';
import App from './App';

test('table renders with headers', async () => {
  render(<App />);
  await waitFor(() => screen.getByRole('table'))
  const uuidHeader = screen.getByText(/Uuid/g);
  expect(uuidHeader).toBeInTheDocument();
  const nameHeader = screen.getByText(/Name/g);
  expect(nameHeader).toBeInTheDocument();
  const emailHeader = screen.getByText(/Email/g);
  expect(emailHeader).toBeInTheDocument();
  const requestedAmountHeader = screen.getByText(/Requested Amount/g);
  expect(requestedAmountHeader).toBeInTheDocument();
  const paymentAmountHeader = screen.getByText(/Payment Amount/g);
  expect(paymentAmountHeader).toBeInTheDocument();
  const paymentMethodHeader = screen.getByText(/Payment Method/g);
  expect(paymentMethodHeader).toBeInTheDocument();
  const initiatePaymentHeader = screen.getByText(/Initiate Payment/g);
  expect(initiatePaymentHeader).toBeInTheDocument();
});
/*Second Challenge, only for eligible users, created an if statement to check
  if requested amount is not $0 and payment amount has not being paid then show pay button
*/
test("only for eligible users", async () => {
  render(<App />);
  await waitFor(() => screen.getByRole("table"));
  const rows = screen.getAllByRole("row");
  rows.shift();
  rows.forEach((row) => {
    const cells = within(row).getAllByRole("cell");
    const requestedAmount = cells[3];
    const paymentAmount = cells[4];
    const payButton = within(row).queryByRole("paybutton");
    if (requestedAmount) {
      if (requestedAmount.textContent != "$0" && !paymentAmount.textContent) {
        expect(payButton).toBeInTheDocument();
      } else {
        expect(payButton).toBeNull();
      
      }
    }
  });
});

